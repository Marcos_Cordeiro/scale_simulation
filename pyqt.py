import sys
import socket
import threading
import datetime

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QPushButton
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QDialogButtonBox
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QPlainTextEdit, QTabWidget

class Dialog(QDialog):

    data_send = ''
    stop_threads = False

    def __on_click(self):
        return_msg = self.__fortmat_msg()
        self.__add_text("Sending:" + return_msg)
        self.data_send = return_msg

    def __fortmat_msg(self):
        id = self.btn_id.displayText()
        status = self.btn_status.displayText()
        weight = self.btn_weight.displayText()
        unit = self.btn_unit.displayText()
        return id.ljust(2) + ' ' + status.ljust(1) + ' ' + weight.ljust(10) + ' ' + unit.ljust(5)

    def create_form(self):
        self.btn_id = QLineEdit()
        self.btn_status = QLineEdit()
        self.btn_weight = QLineEdit()
        self.btn_unit = QLineEdit()
        self.txt_ip = QLineEdit("127.0.0.1")
        self.txt_port = QLineEdit("10000")
        self.dlgLayout = QVBoxLayout()
        self.formLayout1 = QFormLayout()
        self.formLayout2 = QFormLayout()
        w = QWidget()

        w.resize(640, 480)
        self.textBox = QPlainTextEdit(w)
        self.textBox.setStyleSheet("QPlainTextEdit { background-color : black; color : white; }")
        self.textBox.setReadOnly(True)

        self.setWindowTitle('Scale Simulation')
        
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tabs.resize(300,200)
        self.tabs.addTab(self.tab1,"Toledo Scale")
        self.tabs.addTab(self.tab2,"Configuration")
        self.tab1.layout = QVBoxLayout(self)

        self.formLayout1.addRow('ID:', self.btn_id)
        self.formLayout1.addRow('Status:',self.btn_status)
        self.formLayout1.addRow('Weight Value:', self.btn_weight)
        self.formLayout1.addRow('Unit:', self.btn_unit)
        self.formLayout1.addRow(self.textBox)
        
        self.tab1.layout.addLayout(self.formLayout1)
        self.tab1.setLayout(self.tab1.layout)
        

        self.tab2.layout = QVBoxLayout(self)

        self.formLayout2.addRow('IP:', self.txt_ip)
        self.formLayout2.addRow('Porta:',self.txt_port)

        self.tab2.layout.addLayout(self.formLayout2)

        self.btn_reset = QPushButton('Reset')
        self.btn_reset.clicked.connect(self.skt_reset)
        # self.tab2.layout.addWidget(self.btn_reset)
        self.tab2.setLayout(self.tab2.layout)

        self.dlgLayout.addWidget(self.tabs)
        self.setLayout(self.dlgLayout)

        
        self.btns = QPushButton('Send')
        self.btns.clicked.connect(self.__on_click)
        self.dlgLayout.addWidget(self.btns)

        self.setLayout(self.dlgLayout)


    def __add_text(self,text):
            self.textBox.appendPlainText(text)
            self.textBox.moveCursor(QtGui.QTextCursor.End)


    def socket_server(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ip = self.txt_ip.displayText()
        port = int(self.txt_port.displayText())
        server_address = (ip, port)
        self.__add_text('starting up on {} port {}'.format(*server_address))
        sock.bind(server_address)
        sock.listen(1)

        while True:
            if self.stop_threads:
                break
            
            self.__add_text('Waiting for a connection')
            connection, client_address = sock.accept()
            try:
                self.__add_text('Connection from' + str(client_address))

                while True:
                    data = connection.recv(50)
                    self.__add_text('received {!r}'.format(data))
                    if (self.data_send != ''):
                        self.__add_text('sending data back to the client')
                        connection.sendall(bytes(self.data_send, 'utf-8'))
                        self.data_send = ''
                    else:
                        self.__add_text('no data from' + str(client_address))
                        break

            finally:
                connection.close()

    def skt_start(self):
        self.socket_thread = threading.Thread(target=self.socket_server)  
        self.socket_thread.start()
    
    def skt_reset(self):
        self.stop_threads = True
        self.socket_thread.join()
        self.socket_server()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.create_form()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    dlg = Dialog()
    dlg.show()
    dlg.skt_start()
    print('thread killed')
    sys.exit(app.exec_())
